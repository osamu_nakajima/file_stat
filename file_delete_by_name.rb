require 'date'

today = Date.today
three_month_ago = today << 2
p three_month_ago
y = three_month_ago.year
m = three_month_ago.month
last_day_three_month_ago = Date.new(y, m, -1)
keep_file_date = last_day_three_month_ago.strftime('%Y%m%d')
delete_file_date = last_day_three_month_ago.strftime('%Y%m')
puts keep_file_date
puts delete_file_date
delete_files = Dir.glob("./*#{delete_file_date}*")
p delete_files
final_delete_files = delete_files.delete_if{|f| f =~ /#{keep_file_date}/ }
p final_delete_files

# FileUtils.rm(final_delete_files)

# get backup files 2 month ago
# keep last date file
