require "fileutils"

files = Dir.glob("./*.txt")

p "####################"
puts files
sorted_files = files.sort_by{|f| File.stat(f).mtime }
p "####################"
puts sorted_files
sorted_files.pop # delete last one
p "####################"
puts sorted_files

FileUtils.rm(sorted_files)
